hosts = {
  gateway: { ip: '10.0.0.10' },
  web1: { ip: '10.0.0.11' },
  web2: { ip: '10.0.0.12' },
}

groups = {
  proxy: ['gateway'],
  web: ['web1', 'web2'],
}

Vagrant.configure('2') do |config|
  config.vm.box = 'debian/buster64'

  hosts.each do |hostname, vars|
    config.vm.define hostname do |host|
      host.vm.provider 'virtualbox' do |vb|
        vb.name = "ask_#{hostname}"
      end

      host.vm.synced_folder '.', '/vagrant', disabled: true

      host.vm.hostname = hostname
      host.vm.network 'private_network', ip: vars[:ip]
      if hostname == :gateway
        host.vm.network 'forwarded_port', guest: 80, host: 8800
      end
    end
  end

  config.vm.provision 'ansible' do |ansible|
    ansible.host_vars = hosts
    ansible.groups = groups
    ansible.playbook = 'playbook.yml'
    ansible.playbook_command = 'true' # only generate inventory
  end
end
