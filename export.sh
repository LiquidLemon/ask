#!/bin/bash
set -exu

VMS=$(vboxmanage list vms | grep ask | cut -d" " -f1 | tr -d \")
vboxmanage export $VMS -o project.ova
