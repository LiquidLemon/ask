default: provision

ANSIBLE_SOURCES := playbook.yml $(shell find roles -type f,l)

provision: vagrant $(ANSIBLE_SOURCES)
	ansible-playbook -i inventory.ini playbook.yml

vagrant: Vagrantfile
	vagrant destroy -f
	vagrant up

.PHONY: clean

clean:
	vagrant destroy -f
