# ASK

The goal of this project was to create a simple environment web applications
behind a load-balancing proxy.

## Nodes

### gateway

The gateway node houses the Nginx server and proxies requests to the web nodes.

### web

Web nodes all expose two basic Java and .NET applications using their builtin
servers.

## Deployment

To deploy this environemnt make sure you have Vagrant, VirtualBox and Ansible
installed.

Execute:

```shell
$ vagrant up
$ ansible-playbook -i inventory.ini playbook.yml
```

To export all the machines as a VritualBox appliance run `./export.sh`.
